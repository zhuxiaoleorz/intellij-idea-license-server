FROM buildpack-deps:jessie-scm

# 签名
MAINTAINER saymagic "zhuxiaole@zhuxiaole.org"

RUN apt-get update

ADD IntelliJIDEALicenseServer_linux_amd64 /IntelliJIDEALicenseServer_linux_amd64
ADD IntelliJIDEALicenseServer.html /IntelliJIDEALicenseServer.html
RUN chmod a+x /IntelliJIDEALicenseServer_linux_amd64

ENV IDEA_USER_NAME idea_user
ENV PRO_LONGATION_PERIOD 3600000

EXPOSE 1027

CMD ["./IntelliJIDEALicenseServer_linux_amd64"]